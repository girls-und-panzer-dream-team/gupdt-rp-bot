import { GuildMember } from "discord.js";

export class GuPTank {
    tankId:     string;
    tankStatId: string;
    tankDamage: number;
    tankValue:  number;

    commander:     GuildMember;
    commanderAi:   boolean;
    commanderName: string;
    commanderXp:   number;

    armorSave:    number;
    armorValue:   number;

    cannonPen:    number;

    hitThreshold:         number;
    hitThresholdModified: number;

    lastTacticalAction:   string;
} 