import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";
import { WeatherMainService } from "../../weathersystem/weather-main-service";
import { ActivityMainService } from "../../activitysystem/activity-main-service";

export const DebugChannelsCommand = new Command()
  .match(matchPrefixes(""))
  .use(async (context) => {
    const { manager, message } = context;
    if (message.member.roles.cache.has("592843915594563585")) {
        let textChannel: TextChannel = message.channel as TextChannel;
        if (textChannel.topic != null && textChannel.topic.includes("[management]")) {
            let weatherService = manager.getService(WeatherMainService);
            let activityService = manager.getService(ActivityMainService);


            let returnMessage = "**REGISTERED CHANNELS**\n"
            returnMessage += "[**WEATHER**]\n"

            weatherService.channels.forEach(channel => {
                returnMessage += "\t--" + channel.name + "\n"
            })
            returnMessage += "\n[**RP**]\n"
            let sortChannels = activityService.channels.sort((a: TextChannel, b: TextChannel) => {
                if (a.parent.rawPosition > b.parent.rawPosition) {
                    return 1;
                }
                if (a.parent.rawPosition < b.parent.rawPosition) {
                    return -1;
                }
                if (a.rawPosition > b.rawPosition) {
                    return 1;
                }
                if (a.rawPosition < b.rawPosition) {
                    return -1;
                }
                return 0;
            })
            let categoryName = "";
            sortChannels.forEach(channel => {
                if (categoryName != channel.parent.name) {
                    returnMessage += "- **" + channel.parent.name + "**\n"
                    categoryName = channel.parent.name
                }
                if (returnMessage.length > 1500) {
                    message.channel.send(returnMessage)
                    returnMessage = "";
                }
                returnMessage += "\t--" + channel.name + "\n"
            })

            message.channel.send(returnMessage)
        }
    }
  });
