import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { DebugChannelsCommand } from "./commands/debug-channels-command"

export const DebugCommandGroup = new CommandGroup()
  .match(matchPrefixes(".debug"))
  .setCommands(DebugChannelsCommand)