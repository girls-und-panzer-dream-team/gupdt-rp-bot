import { Adapter, Bot } from "@enitoni/gears-discordjs"
import { token } from "../config.json"
import { Connection, createConnection } from "typeorm"
import { WeatherMainService } from "./weathersystem/weather-main-service"

import ormConfig from "../ormconfig.json"
import { GuPCharacter } from "./database/entity/gupcharacter"
import { GuPSchool } from "./database/entity/gupschool"
import { GuPSenshaDoStats } from "./database/entity/gupsenshadostats"
import { GuPTankRecord } from "./database/entity/guptankrecord"
import { TextChannel } from "discord.js"
import { ActivityMainService } from "./activitysystem/activity-main-service"
import { BattleMainService } from "./battlesystem/battle-main-service"
import { HelpCommandGroup } from "./helpsystem/help-command-group"
import { DebugCommandGroup } from "./debugsystem/debug-command-group"

const adapter = new Adapter({ token })
const bot = new Bot({
    adapter,
    commands: [HelpCommandGroup, DebugCommandGroup],
    services: [WeatherMainService, ActivityMainService, BattleMainService] 
})

async function main() {
  await bot.start()
  console.log("BOT IS RUNNING")
  setTimeout(() => {
    console.log("READY - INITIALIZING")
    console.log("GATHERING CHANNELS")
    bot.client.guilds.cache.forEach(guild => {
      guild.channels.cache.forEach(channel => {
        if (channel.parent != null && channel.type === "text") {
          let textChannel = channel as TextChannel
          if (textChannel.topic != null && textChannel.topic.includes("[rp]")) {
            bot.manager.getService(ActivityMainService).channels.push(textChannel);
          }
          if (textChannel.topic != null && textChannel.topic.includes("[weather]")) {
            console.log("Initialize Weather Tick")
            bot.manager.getService(WeatherMainService).channels.push(textChannel);
          }
        }
      })
    })

    // INITIALIZE WEATHER SYSTEM
    console.log("INTIALIZING WEATHER SYSTEM")
    bot.manager.getService(WeatherMainService).startChannelTick();
  }, 5000);

  const connection: Connection = await createConnection({
    type: "mysql",
    host: ormConfig.host,
    port: 3306,
    username: ormConfig.username,
    password: ormConfig.password,
    database: ormConfig.database,
    entities: [GuPCharacter, GuPSchool, GuPSenshaDoStats, GuPTankRecord],
    synchronize: true,
  });
}

main()
