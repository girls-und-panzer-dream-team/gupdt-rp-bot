import { Column, OneToMany, OneToOne, PrimaryColumn } from "typeorm";
import { GuPSenshaDoStats } from "./gupsenshadostats";
import { GuPTankRecord } from "./guptankrecord";

export class GuPCharacter {
    @PrimaryColumn()
    id:            string;

    @Column()
    schoolId:      string;

    @Column({type: "float"})
    money:         number;

    @OneToOne(type => GuPSenshaDoStats, stats => stats.owner, {cascade: true, eager: true})
    senshaDoStats: GuPSenshaDoStats;

    @OneToMany(type => GuPTankRecord, tank => tank.owner, { cascade: true, eager: true})
    tanks: GuPTankRecord[];
}
