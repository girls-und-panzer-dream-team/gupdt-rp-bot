import { Column, OneToMany, PrimaryColumn } from "typeorm";
import { GuPTankRecord } from "./guptankrecord";

export class GuPSchool {
    @PrimaryColumn()
    id:    string;

    @Column({type: "float"})
    score: number;

    @OneToMany(type => GuPTankRecord, record => record.school, { cascade: true, eager: true})
    tanks: GuPTankRecord[];
}