import { Column, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { GuPCharacter } from "./gupcharacter";
import { GuPSchool } from "./gupschool";

export class GuPTankRecord {
    @PrimaryGeneratedColumn()
    id:         string;

    @ManyToOne(type => GuPSchool, school => school.tanks)
    school:     GuPSchool;
    
    @ManyToOne(type => GuPCharacter, character => character.tanks)
    owner:      GuPCharacter;
    
    @Column()
    tankStatId: string;
    
    @Column()
    damage:     number;
    
    @Column()
    available:  boolean;
}