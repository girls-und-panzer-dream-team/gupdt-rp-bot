import { Column, ManyToOne, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { GuPCharacter } from "./gupcharacter";

export class GuPSenshaDoStats {
    @PrimaryGeneratedColumn()
    id:                 number;

    @OneToOne(type => GuPCharacter, character => character.senshaDoStats)
    owner:              GuPCharacter;
    
    @Column()
    battlesNum:         number;
    
    @Column()
    battlesWon:         number;
    
    @Column()
    battlesLost:        number;
    
    @Column()
    tanksDisabled:      number;
    
    @Column()
    tanksLost:          number;
    
    @Column({type: "float"})
    senshadoExperience: number;
}
