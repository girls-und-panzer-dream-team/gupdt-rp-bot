import { Command } from "@enitoni/gears-discordjs";
import { matchPrefixes } from "@enitoni/gears";
import { TextChannel } from "discord.js";

export const HelpCommand = new Command()
  .match(matchPrefixes(""))
  .use(async (context) => {
    const { manager, message } = context;
    let textChannel: TextChannel = message.channel as TextChannel;
    if (textChannel.topic != null && textChannel.topic.includes("[help]")) {
        message.channel.send("**This Bot is designed by Natalie#1702 for use with the Girls und Panzer Dream Team Roleplay**\n"+
      "The Bot offers functions for several areas:\n" +
      "Please leave feedback or bug reports in #gupdt-rp-bot-feedback")
    }
  });
