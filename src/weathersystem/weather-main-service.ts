import { Service } from "@enitoni/gears-discordjs";
import { TextChannel } from "discord.js";
import cron from "cron";
import fetch from "node-fetch";
import weatherconfig from "../../weatherconfig.json";

export class WeatherMainService extends Service {
    channels: TextChannel[] = [];

    startChannelTick() {
        console.log("Started Weather Tick")
        let weatherTick = new cron.CronJob(`${weatherconfig.cronTimer}`, async () => {
            const response = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=36.3167&lon=140.6&&units=metric&exclude=current,minutely,hourly&appid=${weatherconfig.token}`).then(response => response.json());
            let dayResponse = (response.daily[0])

            let dayTs = new Date(dayResponse.dt * 1000);
            let temperatureC = Math.round(dayResponse.temp.day);
            let temperatorF = Math.round(dayResponse.temp.day * 1.8 + 32);

            this.channels.forEach(channel => {
                channel.send(`
\`\`\`Weather for: ${dayTs.getFullYear()}-${dayTs.getMonth() + 1}-${dayTs.getDate()}
Weather:     ${dayResponse.weather[0].main} - ${dayResponse.weather[0].description}
Temperature: ${temperatureC}°C / ${temperatorF}°F
Humidity:    ${dayResponse.humidity}%
Cloudiness:  ${dayResponse.clouds}%
\`\`\`
`)
            })
          }); 
        weatherTick.start();
    }
}
